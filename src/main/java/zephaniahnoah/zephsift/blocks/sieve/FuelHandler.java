package zephaniahnoah.zephsift.blocks.sieve;

import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.IFuelHandler;

public class FuelHandler implements IFuelHandler {

    @Override
    public int getBurnTime(ItemStack fuel) {
        if (fuel.getItem() == SieveBlock.sieve) {
            return 3200;
        }
        return 0;
    }
}