package zephaniahnoah.zephsift.blocks.sieve;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import zephaniahnoah.zephsift.Config;
import zephaniahnoah.zephsift.ModBlocks;
import zephaniahnoah.zephsift.ModSift;

@net.minecraftforge.fml.common.Mod.EventBusSubscriber
public class SieveBlock extends Block implements net.minecraft.block.ITileEntityProvider {

	java.util.ArrayList<ItemStack> siftable = Config.siftables;
	public static final PropertyBool TRIGGERED = PropertyBool.create("triggered");
	public static net.minecraft.item.Item sieve;
	private static final AxisAlignedBB BOUNDING_BOX = new AxisAlignedBB(0, 0.75, 0, 1, 1, 1);
	public static String name = "sieve";
	int itemType;

	@SubscribeEvent
	public static void onBlockRegister(RegistryEvent.Register<Block> e) {
		e.getRegistry().register(ModBlocks.sieveblock);
	}

	@SubscribeEvent
	public static void onItemRegister(RegistryEvent.Register<Item> e) {
		e.getRegistry().register(new net.minecraft.item.ItemBlock(ModBlocks.sieveblock).setRegistryName(ModBlocks.sieveblock.getRegistryName()));
	}

	public SieveBlock() {
		super(net.minecraft.block.material.Material.WOOD);
		setCreativeTab(net.minecraft.creativetab.CreativeTabs.DECORATIONS);
		setUnlocalizedName(ModSift.MODID + "." + name);
		setRegistryName(name);
		this.setHardness(2);
		net.minecraftforge.fml.common.registry.GameRegistry.registerTileEntity(SieveTileEntity.class, ModSift.MODID + ":" + name);
		// FIX RECIPE GameRegistry.addShapedRecipe(new
		// net.minecraftforge.oredict.ShapedOreRecipe(new
		// ItemStack(net.minecraftforge.fml.common.registry.ForgeRegistries.BLOCKS.getValue(new
		// ResourceLocation(ModSift.MODID, name))),new Object[] {"LWL","LWL","L L", 'L',
		// "logWood", 'W', net.minecraft.init.Blocks.WEB}));
	}

	public AxisAlignedBB getBoundingBox(IBlockState blockState, net.minecraft.world.IBlockAccess blockAccess, BlockPos pos) {
		return BOUNDING_BOX;
	}

	@Override
	public SoundType getSoundType(IBlockState state, World world, BlockPos pos, @javax.annotation.Nullable net.minecraft.entity.Entity entity) {
		return SoundType.WOOD;
	}

	@net.minecraftforge.fml.relauncher.SideOnly(net.minecraftforge.fml.relauncher.Side.CLIENT)
	public void initModel() {
		net.minecraftforge.fml.client.registry.ClientRegistry.bindTileEntitySpecialRenderer(SieveTileEntity.class, new SieveTESR());
	}

	@net.minecraftforge.fml.relauncher.SideOnly(net.minecraftforge.fml.relauncher.Side.CLIENT)
	public void initItemModel() {
		net.minecraftforge.client.model.ModelLoader.setCustomModelResourceLocation(net.minecraft.item.Item.getItemFromBlock(this), 0, new net.minecraft.client.renderer.block.model.ModelResourceLocation(getRegistryName(), "inventory"));
	}

	@Override
	public net.minecraft.tileentity.TileEntity createNewTileEntity(World worldIn, int meta) {
		return new SieveTileEntity();
	}

	@Override
	public boolean isBlockNormalCube(IBlockState blockState) {
		return false;
	}

	@Override
	public boolean isOpaqueCube(IBlockState blockState) {
		return false;
	}

	@Override
	public boolean isFullCube(IBlockState blockState) {
		return false;
	}

	@Override
	public BlockRenderLayer getBlockLayer() {
		return BlockRenderLayer.CUTOUT;
	}

	private SieveTileEntity getTE(World world, BlockPos pos) {
		return (SieveTileEntity) world.getTileEntity(pos);
	}

	@Override
	public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos) {
		worldIn.scheduleUpdate(pos, this, 0);
	}

	@Override
	public void updateTick(World world, BlockPos pos, IBlockState state, Random rand) {
		if (!world.isRemote) {
			/*
			 * Trying to automate blocks being placed in the sieve. This method is probably best findItem(world, pos); This line is CRAP! BlockPos pos1 = new BlockPos(pos.getX(), pos.getY()+1, pos.getZ()); IBlockState block = world.getBlockState(pos1); System.out.println(new ItemStack(block.getBlock().getItemDropped(block, null, 0)));
			 */
			if (world.isBlockIndirectlyGettingPowered(pos) > 0) {
				sift(world, pos);
			}
		}
	}

	public void onBlockHarvested(World world, BlockPos pos, IBlockState state, EntityPlayer player) {
		if (!world.isRemote) {
			SieveTileEntity te = getTE(world, pos);
			if (te.getStack() != null) {
				EntityItem entity = new EntityItem(world, pos.getX() + 0.5, pos.getY() - 0.2, pos.getZ() + 0.5, new ItemStack(te.getStack().getItem(), 1, te.getStack().getItemDamage()));
				world.spawnEntity(entity);
			}
		}
	}

	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, net.minecraft.util.EnumHand hand, net.minecraft.util.EnumFacing side, float hitX, float hitY, float hitZ) {
		if (!world.isRemote) {
			if (getTE(world, pos).getStack() == null) {
				setInput(player.getHeldItemMainhand(), player, world, pos);
			} else {
				sift(world, pos);
			}
		}
		return true;
	}

	public void sift(World world, BlockPos pos) {
		SieveTileEntity te = getTE(world, pos);
		ItemStack stack = te.getStack();
		if (stack != null) {
			IBlockState state = Block.getBlockFromItem(stack.getItem()).getStateFromMeta(stack.getMetadata());
			if (world.isRemote) {
				net.minecraft.client.Minecraft.getMinecraft().effectRenderer.addBlockDestroyEffects(pos, state);
			}
			if (te.getState() > 1) {
				te.decrement();
				te.updateState();
			} else {
				for (int i = 0; i < siftable.size(); i++) {
					if (stack.getItem().equals(siftable.get(i).getItem())) {
						if (siftable.get(i).getItem().getDamage(siftable.get(i)) == stack.getItem().getDamage(stack)) {
							itemType = i;
							break;
						}
					}
				}

				int length = Config.siftedItems.get(itemType).length;

				int rand = new Random().nextInt(length);

				ItemStack[] arr = Config.siftedItems.get(itemType);

				ItemStack output = arr[rand];

				if (output == null) {
					System.out.println("Output is null!");
				}

				// Something null here sometimes
				EntityItem entity = new EntityItem(world, pos.getX() + 0.5, pos.getY() - 0.2, pos.getZ() + 0.5, new ItemStack(output.getItem(), 1, output.getItemDamage()));

				world.spawnEntity(entity);
				te.setStack(null, 0);
			}
		}
	}

	public void setInput(ItemStack itemToCheck, EntityPlayer player, World world, BlockPos pos) {
		SieveTileEntity te = getTE(world, pos);
		boolean is = false;
		for (int i = 0; i < siftable.size(); i++) {
			if (itemToCheck.getItem().equals(siftable.get(i).getItem()) && itemToCheck.getItem().getDamage(itemToCheck) == siftable.get(i).getItem().getDamage(siftable.get(i))) {
				te.setStack(siftable.get(i).getItem(), siftable.get(i).getItem().getDamage(siftable.get(i)));
				if (player != null) {
					player.openContainer.detectAndSendChanges();
					if (!player.isCreative()) {
						itemToCheck.setCount(itemToCheck.getCount() - 1);
					}
				}
			}
		}
	}
}
