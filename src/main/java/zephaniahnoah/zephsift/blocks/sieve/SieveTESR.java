package zephaniahnoah.zephsift.blocks.sieve;

import net.minecraft.client.renderer.GlStateManager;

@net.minecraftforge.fml.relauncher.SideOnly(net.minecraftforge.fml.relauncher.Side.CLIENT)
public class SieveTESR extends net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer<SieveTileEntity> {

    @Override
    public void render(SieveTileEntity te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        GlStateManager.pushAttrib();
        GlStateManager.pushMatrix();

        GlStateManager.translate(x, y, z);
        GlStateManager.disableRescaleNormal();

        renderItem(te);

        GlStateManager.popMatrix();
        GlStateManager.popAttrib();
    }

    private void renderItem(SieveTileEntity te) {
        net.minecraft.item.ItemStack stack = te.getStack();
        if (stack != null) {
            int state = te.getState();
            net.minecraft.client.renderer.RenderHelper.enableStandardItemLighting();
            GlStateManager.enableLighting();
            GlStateManager.pushMatrix();

            GlStateManager.translate(.5, .817 + state * 0.015 / 2, .5);
            GlStateManager.scale(.75f, state * 0.015, .75f);

            net.minecraft.client.Minecraft.getMinecraft().getRenderItem().renderItem(stack, net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType.NONE);

            GlStateManager.popMatrix();
        }
    }
}
