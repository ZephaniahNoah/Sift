package zephaniahnoah.zephsift.proxy;

import zephaniahnoah.zephsift.ModBlocks;
import zephaniahnoah.zephsift.ModSift;

@net.minecraftforge.fml.common.Mod.EventBusSubscriber(value = net.minecraftforge.fml.relauncher.Side.CLIENT, modid = ModSift.MODID)
public class ClientProxy extends CommonProxy {

    @Override
    public void preInit(net.minecraftforge.fml.common.event.FMLPreInitializationEvent e) {
        super.preInit(e);
        net.minecraftforge.client.model.obj.OBJLoader.INSTANCE.addDomain(ModSift.MODID);
        ModBlocks.initModels();
    }

    @Override
    public void init(net.minecraftforge.fml.common.event.FMLInitializationEvent e) {
        super.init(e);
    }

    @net.minecraftforge.fml.common.eventhandler.SubscribeEvent
    public static void registerModels(net.minecraftforge.client.event.ModelRegistryEvent e) {
        ModBlocks.initItemModels();
    }
}
