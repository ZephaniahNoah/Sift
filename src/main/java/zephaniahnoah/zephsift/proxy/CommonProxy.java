package zephaniahnoah.zephsift.proxy;

import net.minecraftforge.common.config.Configuration;
import zephaniahnoah.zephsift.ModSift;

import java.io.File;

public class CommonProxy {

    public Configuration config;

    public void preInit(net.minecraftforge.fml.common.event.FMLPreInitializationEvent e) {
        File directory = e.getModConfigurationDirectory();
        config = new Configuration(new File(directory.getPath(), ModSift.MODID + ".cfg"));
        zephaniahnoah.zephsift.network.PacketHandler.registerMessages(ModSift.MODID);
        zephaniahnoah.zephsift.ModBlocks.init();
    }

    public void postInit(net.minecraftforge.fml.common.event.FMLPostInitializationEvent e) {
        zephaniahnoah.zephsift.Config.readConfig(e, config);
        if (config.hasChanged()) {
            config.save();
        }
    }

    public void init(net.minecraftforge.fml.common.event.FMLInitializationEvent e) {
    }
}
