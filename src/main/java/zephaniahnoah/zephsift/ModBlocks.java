package zephaniahnoah.zephsift;

import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import zephaniahnoah.zephsift.blocks.sieve.SieveBlock;

@EventBusSubscriber
public class ModBlocks {

    public static SieveBlock sieveblock;

    public static void init() {
        sieveblock = new SieveBlock();
    }

    @SideOnly(Side.CLIENT)
    public static void initModels() {
        sieveblock.initModel();
    }

    @SideOnly(Side.CLIENT)
    public static void initItemModels() {
        sieveblock.initItemModel();
    }
}
