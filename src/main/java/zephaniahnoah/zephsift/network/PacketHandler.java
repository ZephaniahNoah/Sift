package zephaniahnoah.zephsift.network;

public class PacketHandler {
    private static int packetId = 0;

    public static net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper INSTANCE = null;

    public static int nextID() {
        return packetId++;
    }

    public static void registerMessages(String channelName) {
        INSTANCE = net.minecraftforge.fml.common.network.NetworkRegistry.INSTANCE.newSimpleChannel(channelName);
    }
}
