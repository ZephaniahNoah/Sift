package zephaniahnoah.zephsift;

import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.Logger;

@Mod(modid = ModSift.MODID, name = ModSift.MODNAME, version = ModSift.MODVERSION, dependencies = "required-after:forge@[13.20.1.2386,)", useMetadata = true)
@Mod.EventBusSubscriber
public class ModSift {

    public static final String MODID = "zephsift";
    public static final String MODNAME = "Sift";
    public static final String MODVERSION = "1.5";

    @net.minecraftforge.fml.common.SidedProxy(clientSide = "zephaniahnoah.zephsift.proxy.ClientProxy", serverSide = "zephaniahnoah.zephsift.proxy.ServerProxy")
    public static zephaniahnoah.zephsift.proxy.CommonProxy proxy;

    @Mod.Instance
    public static ModSift instance;

    public static Logger logger;

    @Mod.EventHandler
    public void preInit(net.minecraftforge.fml.common.event.FMLPreInitializationEvent event) {
        logger = event.getModLog();
        proxy.preInit(event);
        net.minecraftforge.fml.common.registry.GameRegistry.registerFuelHandler(new zephaniahnoah.zephsift.blocks.sieve.FuelHandler());
    }

    @Mod.EventHandler
    public void init(net.minecraftforge.fml.common.event.FMLInitializationEvent e) {
        proxy.init(e);
    }

    @Mod.EventHandler
    public void postInit(net.minecraftforge.fml.common.event.FMLPostInitializationEvent e) {
        proxy.postInit(e);
    }
}
