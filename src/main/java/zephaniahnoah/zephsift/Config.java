package zephaniahnoah.zephsift;

import java.util.ArrayList;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class Config {

	public static boolean syntaxError = false;
	private static String[] siftList;
	public static ArrayList<ItemStack[]> siftedItems = new ArrayList<ItemStack[]>();
	public static ArrayList<ItemStack> siftables = new ArrayList<ItemStack>();

	public static void readConfig(net.minecraftforge.fml.common.event.FMLPostInitializationEvent e, Configuration config) {
		config.load();

		siftList = "minecraft:sand:0;minecraft:gravel:0".split(";");
		siftList = config.getStringList("inputs", Configuration.CATEGORY_GENERAL, siftList, "These are the items/blocks that can be placed in the sift.");

		if (config.hasChanged()) {
			config.save();
		}

		String[] defaults = "minecraft:flint:0;minecraft:stick:0".split(";");
		loop: for (int i = 0; i < siftList.length; i++) {
			siftables.add(parse(siftList[i]));
			String[] idsFromConfig = config.getStringList(i + "", "outputs", defaults, "");

			ItemStack[] outputs = new ItemStack[idsFromConfig.length];

			for (int j = 0; j < idsFromConfig.length; j++) {
				String unparsedItemStack = idsFromConfig[j];
				if (unparsedItemStack.contains(":")) {
					outputs[j] = parse(unparsedItemStack);
				} else {
					syntaxError = true;
					break loop;
				}
			}
			siftedItems.add(outputs);
		}
	}

	public static ItemStack parse(String unparsedItemStack) {
		String[] split = unparsedItemStack.split(":");
		if (split.length < 3) {
			return ItemStack.EMPTY;
		}
		Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(split[0], split[1]));
		if (item == null) {
			item = Item.getItemFromBlock(ForgeRegistries.BLOCKS.getValue(new ResourceLocation(split[0], split[1])));
		}
		return new ItemStack(item, 1, Integer.parseInt(split[2]));
	}
}
